# Tic Tac Toe 64

This is a Nintendo 64 port of [Tic Tac Toe in C](https://gitlab.com/n64squid/tic-tac-toe).

<img src="images/tic-tac-toe-64-1.png" width="49%"/> <img src="images/tic-tac-toe-64-2.png" width="49%"/>

## Features

* ASCII / Command line graphics
* 1-player mode versus AI
* 2-player mode with one or two controllers
* Adjustable board size from 1x1 to 9x9
	* 4x4 to 9x9 boards give you multiple cells per turn
* Runs on real N64 hardware

Tested on Ares emulator and N64 using an Everdrive V2.