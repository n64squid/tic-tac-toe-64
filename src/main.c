#include <stdio.h>
#include <libdragon.h>
#include <string.h>
#include <stdlib.h>

// Some common definitions
#define X 88
#define O 79
#define SPACE 32
#define TIE 1

// Default settings
#define DEFAULT_PLAYERS 1
#define DEFAULT_SIZE 3
#define PLAYER_1 0
#define PLAYER_2 1
#define MAX_PLAYERS 2
#define MAX_BOARD_SIZE 9
#define MIN_BOARD_SIZE 2

// This is the list of stages at which the game can be at at any point
typedef enum {
	STAGE_TITLE_SCREEN,
	STAGE_PLAYER_SELECT,
	STAGE_SIZE_SELECT,
	STAGE_GAME
} stage_t;

char text_buffer[CONSOLE_WIDTH*CONSOLE_HEIGHT*sizeof(char)] = {0};

unsigned char get_player_token(unsigned char player) {
	return player == PLAYER_1 ? X : O;
}

unsigned char get_offset(unsigned char size, unsigned char x, unsigned char y) {
	return size*x + y;
}

unsigned char check_win(unsigned char* board, unsigned char s) {

	int i,j;		// Loop iterators
	int hx,ho;		// Horizontal counters
	int vx,vo;		// Vertical counters
	int hval, vval;	// Temp storage for stuff
	int dx1, dx2, do1, do2;
	int total = 0;
	dx1 = dx2 = do1 = do2 = 0;

	// Loop through rows and columns
	for (i = 0; i < s; i++) {
		// Reset
		hx = ho = vx = vo = 0;
		// Loop through cells
		for (j = 0; j < s; j++) {
			// Get values for rows and columns
			hval = board[get_offset(s, j, i)];
			vval = board[get_offset(s, i, j)];
			// Check for matches
			if (hval == X) {
				hx ++;
			}
			if (hval == O) {
				ho ++;
			}
			if (vval == X) {
				vx ++;
			}
			if (vval == O) {
				vo ++;
			}
			// Check diagonals
			// Downwards
			if (i == j) {
				if (hval == X) {
					dx1 ++;
				}
				if (hval == O) {
					do1 ++;
				}
			}
			// Upwards
			if (s - 1 - i == j) {
				if (hval == X) {// It's an X
					dx2 ++;
				}
				if (hval == O) {// It's an O
					do2 ++;
				}
			}
		}
		if (hx == s || vx == s || dx1 == s || dx2 == s) {
			return X;
		} else if (ho == s || vo == s || do1 == s || do2 == s) {
			return O;
		}
		total += hx + ho;
	}
	// Check if board is full
	// printf("Total: %i",total);
	if (total == s * s) {
		return 1;
	}
	return 0;
}

void bot_turn(unsigned char* board, char player, int size) {
	int i,j;		// Loop iterators
	int c;			// Secondary loop iterator
	int cv,l;		// Secondary loop counter
	int mult;		// Score multiplier
	int value;		// Cell's current value in loop
	int score;		// Cell's priority
	int highscore=0;// Cell with the highest score.
					// Byte 1 is blank, 2 is x value, 3 is y value, 4 is score
	int selfScore=0;// Count of the bot's own filled cells

	// Loop through rows
	for (i = 0; i < size; i++) {
		// Loop through cells
		for (j = 0; j < size; j++) {
			value = *(board + (sizeof(char)*i*size) + (sizeof(char)*j));
			if (value == SPACE) {
				// If cell has not been chosen yet

				// Get coordinate score
				score = 1 + 1; 						 // Add one for each row/column
				if (i == j) 			{ score++; } // Add one for diagonal 1
				if (size - 1 - i == j) 	{ score++; } // Add one for diagonal 2

				mult = 1 + (1<<8) + (1<<16) + (1<<24);
				selfScore = 0;
				for (c = 0; c < size; c++) {
					cv = 	(*(board + (sizeof(char)*i*size) + (sizeof(char)*c)) << 24) +		// Horizontal
							(*(board + (sizeof(char)*c*size) + (sizeof(char)*j)) << 16);		// Vertical
					if (i == j) {
						cv += *(board + (sizeof(char)*c*size) + (sizeof(char)*c)) << 8;			// Diagonal 1
					}
					if (size - 1 - i == j) {
						cv += *(board + (sizeof(char)*(size - 1-c)*size) + (sizeof(char)*c));	// Diagonal 2
					}
							
					// Loop through the 4 parameters
					for (l=0;l<4;l++) {
						// Only add score if it it isn't blank or belonging to a the bot
						if ((((cv>>(l*8)) & 0xFF) != 0x20) && (((cv>>(l*8)) & 0xFF) != player) && (((cv>>(l*8)) & 0xFF) != 0)) {
							score += (mult>>(l*8)) & 0xFF;
							mult += 2<<(l*8);
						}
						// Check if there are many of our own cells filled
						if (((cv>>(l*8)) & 0xFF) == player) {
							selfScore += 1<<(l*8);
						}
						// If row/col/diag is missing just one cell, fill it.
						if (selfScore >> (l*8) == (size - 1)) {
							score += 5;
						}
					}
				}
				
				// Check for a high score
				if (score > (highscore & 0xFF)) {
					highscore = (score & 0xFF) | (i << 16 & 0xFF0000) | (j << 8 & 0xFF00);
					// printf("%08x!", highscore);
				} else {
					// printf("%08x ", score & 0xFF);
				}
			} else {
				// If cell has been set, just print who filled it
				//printf("%c        ", value);
			}
		}
	}
	// Set cell
	*(board + (sizeof(char)*(highscore >> 16 & 0xFF)*size) + (sizeof(char)*(highscore >> 8 & 0xFF))) = player;
}

void draw_board (unsigned char* board, unsigned char s, unsigned char x, unsigned char y) {
	int i, j;
	char temp_buffer[CONSOLE_WIDTH*1*sizeof(char)] = {0};
	for (i=0; i<s; i++) {
		// Print border line
		if (i) {
			for (j=0; j<s; j++) {
				sprintf(temp_buffer, "%s---", j ? "-" : "");
				strcat(text_buffer, temp_buffer);
			}
		}
		strcat(text_buffer, "\n");
		// Print each row of values
		for (j=0; j<s; j++) {
			sprintf(
				temp_buffer,
				"%s%c%c%c",
				j ? "|" : "",
				j == x && i == y ? '[' : ' ',
				board[get_offset(s, i, j)],
				j == x && i == y ? ']' : ' '
				);
			strcat(text_buffer, temp_buffer);
		}
		strcat(text_buffer, "\n");
	}
}

void print_logo(void) {
	strcat(text_buffer," _____ _        _____            _____             __   _  _\n");
	strcat(text_buffer,"|_   _(_) ___  |_   _|_ _  ___  |_   _|__   ___   / /_ | || |\n");
	strcat(text_buffer,"  | | | |/ __|   | |/ _` |/ __|   | |/ _ \\ / _ \\ | '_ \\| || |_\n");
	strcat(text_buffer,"  | | | | (__    | | (_| | (__    | | (_) |  __/ | (_) |__   _|\n");
	strcat(text_buffer,"  |_| |_|\\___|   |_|\\__,_|\\___|   |_|\\___/ \\___|  \\___/   |_|\n");
}

int main(void) {
	// Initialise things
	console_init();
	controller_init();

	// Set modes
	console_set_render_mode(RENDER_MANUAL);

	// Some variables
	unsigned char current_stage = STAGE_TITLE_SCREEN;
	unsigned char current_player = PLAYER_1;
	unsigned char current_controller = PLAYER_1;
	unsigned char player_count = DEFAULT_PLAYERS;
	unsigned char stage_size = DEFAULT_SIZE;
	unsigned char new_frame = 0;
	unsigned char x = 0;
	unsigned char y = 0;
	unsigned char winner = 0;
	unsigned char moves_left = 0;

	// Set colour
	graphics_set_color(
		graphics_make_color(0, 255, 0, 255),
		graphics_make_color(0, 0, 0, 255)
		);

	// Text buffer
	char temp_buffer[CONSOLE_WIDTH*2*sizeof(char)] = {0};

	// Board
	unsigned char* board = NULL;

	// Controllers structure
	struct controller_data controllers;

	// Main loop
	while(1) {
		// Get controller input
		controller_scan();
		controllers = get_keys_down();

		// Keep the logo on top of every frame
		print_logo();

		// Check which stage we're at
		switch (current_stage) {
			case STAGE_TITLE_SCREEN:
				strcat(text_buffer,"\n\t\t\t\t\t\t\t\t\t\t\t\tBy N64 Squid\n");
				strcat(text_buffer,"\nPress A to begin\n");
				if (controllers.c[current_player].A) {
					new_frame = 1; current_stage++;
				}
				break;
			case STAGE_PLAYER_SELECT:
				if (controllers.c[0].up || controllers.c[0].down) {
					// Between 1 and 2
					player_count = ((player_count) % MAX_PLAYERS)+1;
				}
				if (controllers.c[0].up || controllers.c[0].down || new_frame) {
					sprintf(temp_buffer, "\nChoose players: (Up/Down)\n%i\n", player_count);
					strcat(text_buffer, temp_buffer);
				}
				if (controllers.c[current_player].A) {
					new_frame = 1; current_stage++;
				}
				break;
			case STAGE_SIZE_SELECT:
				if (controllers.c[0].up) {
					// Between MIN_BOARD_SIZE and MAX_BOARD_SIZE
					stage_size = stage_size+1 <= MAX_BOARD_SIZE ? stage_size+1 : MIN_BOARD_SIZE;
				} else if (controllers.c[0].down) {
					stage_size = stage_size-1 >= MIN_BOARD_SIZE ? stage_size-1 : MAX_BOARD_SIZE;
				}
				if (controllers.c[0].up || controllers.c[0].down || new_frame) {
					sprintf(temp_buffer, "\nChoose board size: (Up/Down)\n%i\n", stage_size);
					strcat(text_buffer, temp_buffer);
				}
				if (controllers.c[current_player].A) {
					new_frame = 1; current_stage++;
					moves_left = stage_size / 2;
				}
				break;
			case STAGE_GAME:
				// Set the controller to p1 if there is no p2 controller inserted
				current_controller = get_controllers_present() & CONTROLLER_2_INSERTED ? current_player : PLAYER_1;
				// Check input
				if (controllers.c[current_controller].A) {
					if (board[get_offset(stage_size, y, x)] == SPACE) {
						// Set the piece
						board[get_offset(stage_size, y, x)] = get_player_token(current_player);
						// End the turn
						if (moves_left == 1) {
							if (player_count == 1 && !check_win(board, stage_size)) {
								// Play a bot game
								for (moves_left=0; moves_left<stage_size/2; moves_left++) {
									bot_turn(board, O, stage_size);
								}
							} else {
								// Let player 2 play
								current_player = (current_player+1) % MAX_PLAYERS;
							}
							// Reset moves left
							moves_left = stage_size / 2;
						} else {
							moves_left--;
						}
					}
				}
				// Move the cursor around
				if (controllers.c[current_controller].right) {
					x = (x+1) % stage_size;
				} else if (controllers.c[current_controller].left) {
					x = (x-1) % stage_size;
				} else if (controllers.c[current_controller].up) {
					y = (y-1) % stage_size;
				} else if (controllers.c[current_controller].down) {
					y = (y+1) % stage_size;
				}
				// Check for out of bounds
				x = x >= stage_size ? stage_size-1 : x;
				y = y >= stage_size ? stage_size-1 : y;

				// We just started a game, let's build the board
				if (new_frame) {
					board = malloc(stage_size * stage_size * sizeof(char));
					memset(board, SPACE, stage_size * stage_size * sizeof(char));
					new_frame = 0;
				}

				// Draw the board to screen
				draw_board (board, stage_size, x, y);

				// Post-board text stats
				sprintf(temp_buffer, "\nCurrent player: P%i", current_player+1);
				strcat(text_buffer, temp_buffer);
				if (stage_size > 3) {
					sprintf(temp_buffer, ", Moves left: %i\n\n", moves_left);
					strcat(text_buffer, temp_buffer);
				} else {
					sprintf(temp_buffer, "\n\n");
					strcat(text_buffer, temp_buffer);
				}

				// Check for winners
				if ((winner = check_win(board, stage_size))) {
					// We have a winner! Announce it
					sprintf(
						temp_buffer,
						"Winnner: %s! ",
						winner == TIE ? "No one" :
						(winner == X ? "Player 1" : "Player 2")
						);
					strcat(text_buffer, temp_buffer);
					sprintf(temp_buffer, "Press START to play a new game");
					strcat(text_buffer, temp_buffer);

					// Restart the game
					if (controllers.c[0].start) {
						current_stage = STAGE_TITLE_SCREEN;
						current_player = PLAYER_1;
						x = y = 0;
						winner = 0;
						free(board);
					}
				}
				break;
		}

		// Render the screen
		if (text_buffer[0]) {
			console_clear();
			printf(text_buffer);
			memset(text_buffer, '\0', CONSOLE_WIDTH*CONSOLE_HEIGHT*sizeof(char));
			console_render();
		}
	}
}