V=1
SOURCE_DIR=src
BUILD_DIR=build
include $(N64_INST)/include/n64.mk

all: tic-tac-toe-64.z64
.PHONY: all

OBJS = $(BUILD_DIR)/main.o

tic-tac-toe-64.z64: N64_ROM_TITLE="Tic Tac Toe 64"

$(BUILD_DIR)/tic-tac-toe-64.elf: $(OBJS)

clean:
	rm -f $(BUILD_DIR)/* *.z64
.PHONY: clean

-include $(wildcard $(BUILD_DIR)/*.d)